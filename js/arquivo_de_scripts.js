function cliqueMe(){
   alert("O link foi clicado!");    
}

function func1(){
}


function func2(){
}


function func3(){
}

window.onload = function(){
    var link = document.getElementById("item");
    link.addEventListener("click", cliqueMe, false);
    func1();
    func2();
    func3();
}

/*
código equivalemnte ao anterior
function carregamento(){
    var link = document.getElementById("item");
    link.addEventListener("click", cliqueMe, false);
    func1();
    func2();
    func3();
}
window.onload = carregamento;
*/